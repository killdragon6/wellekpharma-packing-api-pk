package model

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"unicode/utf8"

	"github.com/labstack/echo"
)

var DB *sql.DB

func Initialize() {
	db, err := sql.Open("mysql", "root:@/wellekpharma_packing_system")
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Printf("connectDB")
	}
	DB = db
}

// PK
func Getpkwaiting_checklistall(c echo.Context) interface{} {
	Initialize()
	var array []interface{}
	query := "SELECT * FROM `pk_waiting_checklist`"
	rows, err := DB.Query(query)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var pkbillingtime string
		var linepk string
		var pkcustomercode string
		var pkcustomername string
		var pkbillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var pk_status string
		rows.Scan(
			&id,
			&pkbillingtime,
			&linepk,
			&pkcustomercode,
			&pkcustomername,
			&pkbillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&pk_status,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["pkbillingtime"] = pkbillingtime
		elements["linepk"] = linepk
		elements["pkcustomercode"] = pkcustomercode
		elements["pkcustomername"] = pkcustomername
		elements["pkbillnumber"] = pkbillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["pk_status"] = pk_status
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Getpkwaiting_checklistbyid(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	rows, err := DB.Query("select * from pk_waiting_checklist where id = ?", json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var pkbillingtime string
		var linepk string
		var pkcustomercode string
		var pkcustomername string
		var pkbillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var pk_status string
		rows.Scan(
			&id,
			&pkbillingtime,
			&linepk,
			&pkcustomercode,
			&pkcustomername,
			&pkbillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&pk_status,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["pkbillingtime"] = pkbillingtime
		elements["linepk"] = linepk
		elements["pkcustomercode"] = pkcustomercode
		elements["pkcustomername"] = pkcustomername
		elements["pkbillnumber"] = pkbillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["pk_status"] = pk_status
		array = append(array, elements)
	}
	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Adddatatopkwaiting_checklist(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})
	var array []interface{}
	rows, err := DB.Query("INSERT INTO pk_ready_list(`pkbillingtime`, `linepk`, `pkcustomercode`, `pkcustomername`, `pkbillnumber`, `employee_name`, `employee_id`, `localtion`, `count_boxs`, `pk_waiting_checklist_id`, `start_pack`, `end_pack`, `qc_name`, `qc_id`, `pk_status`, `pk_note`,`create_date`, `create_at`, `modify_date`, `modify_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", json_map["pkbillingtime"], json_map["linepk"], json_map["pkcustomercode"], json_map["pkcustomername"], json_map["pkbillnumber"], json_map["employee_name"], json_map["employee_id"], json_map["localtion"], json_map["count_boxs"], json_map["pk_waiting_checklist_id"], json_map["start_pack"], json_map["end_pack"], json_map["qc_name"], json_map["qc_id"], json_map["pk_status"], json_map["pk_note"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"])

	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var pkbillingtime string
		var linepk string
		var pkcustomercode string
		var pkcustomername string
		var pkbillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var pk_waiting_checklist_id string
		var start_pack string
		var end_pack string
		var qc_name string
		var qc_id string
		var pk_status string
		var pk_note string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		rows.Scan(
			&id,
			&pkbillingtime,
			&linepk,
			&pkcustomercode,
			&pkcustomername,
			&pkbillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&pk_waiting_checklist_id,
			&start_pack,
			&end_pack,
			&qc_name,
			&qc_id,
			&pk_status,
			&pk_note,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["pkbillingtime"] = pkbillingtime
		elements["linepk"] = linepk
		elements["pkcustomercode"] = pkcustomercode
		elements["pkcustomername"] = pkcustomername
		elements["pkbillnumber"] = pkbillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["pk_waiting_checklist_id"] = pk_waiting_checklist_id
		elements["start_pack"] = start_pack
		elements["end_pack"] = end_pack
		elements["qc_name"] = qc_name
		elements["qc_id"] = qc_id
		elements["pk_status"] = pk_status
		elements["pk_note"] = pk_note
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}
	res :=
		make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Update_waiting_checklist(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	var array []interface{}
	var stringupdate string

	if json_map["employee_name"] != nil {
		stringupdate += "employee_name = '" + json_map["employee_name"].(string) + "',"
	}
	if json_map["employee_id"] != nil {
		stringupdate += "employee_id = '" + json_map["employee_id"].(string) + "',"
	}
	if json_map["localtion"] != nil {
		stringupdate += "localtion = '" + json_map["localtion"].(string) + "',"
	}
	if json_map["count_boxs"] != nil {
		stringupdate += "count_boxs = '" + json_map["count_boxs"].(string) + "',"
	}
	if json_map["pk_status"] != nil {
		stringupdate += "pk_status = '" + json_map["pk_status"].(string) + "',"
	}
	stringupdate = trimLastChar(stringupdate)
	sqlStatement := `
	UPDATE pk_waiting_checklist
	SET ` + stringupdate + `
	WHERE id = ?`
	rows, err := DB.Query(sqlStatement, json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var pkbillingtime string
		var linepk string
		var pkcustomercode string
		var pkcustomername string
		var pkbillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var pk_status string
		rows.Scan(
			&id,
			&pkbillingtime,
			&linepk,
			&pkcustomercode,
			&pkcustomername,
			&pkbillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&pk_status,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["pkbillingtime"] = pkbillingtime
		elements["linepk"] = linepk
		elements["pkcustomercode"] = pkcustomercode
		elements["pkcustomername"] = pkcustomername
		elements["pkbillnumber"] = pkbillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["pk_status"] = pk_status
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func Getpkready_checklistall(c echo.Context) interface{} {
	Initialize()
	var array []interface{}
	query := "SELECT * FROM `pk_ready_list`"
	rows, err := DB.Query(query)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var pkbillingtime string
		var linepk string
		var pkcustomercode string
		var pkcustomername string
		var pkbillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var pk_waiting_checklist_id string
		var start_pack string
		var end_pack string
		var qc_name string
		var qc_id string
		var pk_status string
		var pk_note string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		rows.Scan(
			&id,
			&pkbillingtime,
			&linepk,
			&pkcustomercode,
			&pkcustomername,
			&pkbillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&pk_waiting_checklist_id,
			&start_pack,
			&end_pack,
			&qc_name,
			&qc_id,
			&pk_status,
			&pk_note,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["pkbillingtime"] = pkbillingtime
		elements["linepk"] = linepk
		elements["pkcustomercode"] = pkcustomercode
		elements["pkcustomername"] = pkcustomername
		elements["pkbillnumber"] = pkbillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["pk_waiting_checklist_id"] = pk_waiting_checklist_id
		elements["start_pack"] = start_pack
		elements["end_pack"] = end_pack
		elements["qc_name"] = qc_name
		elements["qc_id"] = qc_id
		elements["pk_status"] = pk_status
		elements["pk_note"] = pk_note
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Getpkready_checklistbyid(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	rows, err := DB.Query("select * from pk_ready_list where id = ?", json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var pkbillingtime string
		var linepk string
		var pkcustomercode string
		var pkcustomername string
		var pkbillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var pk_waiting_checklist_id string
		var start_pack string
		var end_pack string
		var qc_name string
		var qc_id string
		var pk_status string
		var pk_note string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		rows.Scan(
			&id,
			&pkbillingtime,
			&linepk,
			&pkcustomercode,
			&pkcustomername,
			&pkbillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&pk_waiting_checklist_id,
			&start_pack,
			&end_pack,
			&qc_name,
			&qc_id,
			&pk_status,
			&pk_note,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["pkbillingtime"] = pkbillingtime
		elements["linepk"] = linepk
		elements["pkcustomercode"] = pkcustomercode
		elements["pkcustomername"] = pkcustomername
		elements["pkbillnumber"] = pkbillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["pk_waiting_checklist_id"] = pk_waiting_checklist_id
		elements["start_pack"] = start_pack
		elements["end_pack"] = end_pack
		elements["qc_name"] = qc_name
		elements["qc_id"] = qc_id
		elements["pk_status"] = pk_status
		elements["pk_note"] = pk_note
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}
	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Updatepkready_checklist(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	var array []interface{}
	var stringupdate string

	if json_map["start_pack"] != nil {
		stringupdate += "start_pack = '" + json_map["start_pack"].(string) + "',"
	}
	if json_map["end_pack"] != nil {
		stringupdate += "end_pack = '" + json_map["end_pack"].(string) + "',"
	}
	if json_map["qc_name"] != nil {
		stringupdate += "qc_name = '" + json_map["qc_name"].(string) + "',"
	}
	if json_map["qc_id"] != nil {
		stringupdate += "qc_id = '" + json_map["qc_id"].(string) + "',"
	}
	if json_map["pk_note"] != nil {
		stringupdate += "pk_note = '" + json_map["pk_note"].(string) + "',"
	}
	if json_map["modify_date"] != nil {
		stringupdate += "modify_date = '" + json_map["modify_date"].(string) + "',"
	}
	if json_map["modify_at"] != nil {
		stringupdate += "modify_at = '" + json_map["modify_at"].(string) + "',"
	}
	stringupdate = trimLastChar(stringupdate)
	sqlStatement := `
	UPDATE pk_ready_list
	SET ` + stringupdate + `
	WHERE id = ?`
	rows, err := DB.Query(sqlStatement, json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var pkbillingtime string
		var linepk string
		var pkcustomercode string
		var pkcustomername string
		var pkbillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var pk_waiting_checklist_id string
		var start_pack string
		var end_pack string
		var qc_name string
		var qc_id string
		var pk_status string
		var pk_note string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		rows.Scan(
			&id,
			&pkbillingtime,
			&linepk,
			&pkcustomercode,
			&pkcustomername,
			&pkbillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&pk_waiting_checklist_id,
			&start_pack,
			&end_pack,
			&qc_name,
			&qc_id,
			&pk_status,
			&pk_note,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["pkbillingtime"] = pkbillingtime
		elements["linepk"] = linepk
		elements["pkcustomercode"] = pkcustomercode
		elements["pkcustomername"] = pkcustomername
		elements["pkbillnumber"] = pkbillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["pk_waiting_checklist_id"] = pk_waiting_checklist_id
		elements["start_pack"] = start_pack
		elements["end_pack"] = end_pack
		elements["qc_name"] = qc_name
		elements["qc_id"] = qc_id
		elements["pk_status"] = pk_status
		elements["pk_note"] = pk_note
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func Adddatatosainvoice_checklist(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})
	var array []interface{}
	rows, err := DB.Query("INSERT INTO sa_invoice_checklist(`sabillingtime`, `linesa`, `sacustomercode`, `sacustomername`, `sabillnumber`, `employee_name`, `employee_id`, `localtion`, `count_boxs`, `pk_waiting_checklist_id`, `start_pack`, `end_pack`, `qc_name`, `qc_id`, `sa_status`, `sa_note`,`id_invoice_list`,`create_date`, `create_at`, `modify_date`, `modify_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", json_map["pkbillingtime"], json_map["linepk"], json_map["pkcustomercode"], json_map["pkcustomername"], json_map["pkbillnumber"], json_map["employee_name"], json_map["employee_id"], json_map["localtion"], json_map["count_boxs"], json_map["pk_waiting_checklist_id"], json_map["start_pack"], json_map["end_pack"], json_map["qc_name"], json_map["qc_id"], json_map["pk_status"], json_map["pk_note"], json_map["id_invoice_list"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"])

	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var pkbillingtime string
		var linepk string
		var pkcustomercode string
		var pkcustomername string
		var pkbillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var pk_waiting_checklist_id string
		var start_pack string
		var end_pack string
		var qc_name string
		var qc_id string
		var pk_status string
		var pk_note string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		rows.Scan(
			&id,
			&pkbillingtime,
			&linepk,
			&pkcustomercode,
			&pkcustomername,
			&pkbillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&pk_waiting_checklist_id,
			&start_pack,
			&end_pack,
			&qc_name,
			&qc_id,
			&pk_status,
			&pk_note,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["pkbillingtime"] = pkbillingtime
		elements["linepk"] = linepk
		elements["pkcustomercode"] = pkcustomercode
		elements["pkcustomername"] = pkcustomername
		elements["pkbillnumber"] = pkbillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["pk_waiting_checklist_id"] = pk_waiting_checklist_id
		elements["start_pack"] = start_pack
		elements["end_pack"] = end_pack
		elements["qc_name"] = qc_name
		elements["qc_id"] = qc_id
		elements["pk_status"] = pk_status
		elements["pk_note"] = pk_note
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}
	res :=
		make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func trimLastChar(s string) string {
	r, size := utf8.DecodeLastRuneInString(s)
	if r == utf8.RuneError && (size == 0 || size == 1) {
		size = 0
	}
	return s[:len(s)-size]
}
