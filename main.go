package main

import (
	"main/handler"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {

	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
	}))
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	// PK
	e.GET("/getpkwaiting_checklistall", handler.Getpkwaiting_checklistall)
	e.POST("/getpkwaiting_checklistbyid", handler.Getpkwaiting_checklistbyid)
	e.POST("/adddatatopkwaiting_checklist", handler.Adddatatopkwaiting_checklist)
	e.POST("/update_waiting_checklist", handler.Update_waiting_checklist)

	e.GET("/getpkready_checklistall", handler.Getpkready_checklistall)
	e.POST("/getpkready_checklistbyid", handler.Getpkready_checklistbyid)
	e.POST("/updatepkready_checklist", handler.Updatepkready_checklist)
	e.POST("/adddatatosainvoice_checklist", handler.Adddatatosainvoice_checklist)
	// Port run
	e.Logger.Fatal(e.Start(":7004"))
}
