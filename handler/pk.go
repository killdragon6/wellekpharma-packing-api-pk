package handler

import (
	"main/model"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
)

// Pending Checklis
func Getpkwaiting_checklistall(c echo.Context) error {
	res := model.Getpkwaiting_checklistall(c)
	return c.JSON(http.StatusOK, res)
}

func Getpkwaiting_checklistbyid(c echo.Context) error {
	res := model.Getpkwaiting_checklistbyid(c)
	return c.JSON(http.StatusOK, res)
}

func Adddatatopkwaiting_checklist(c echo.Context) error {
	res := model.Adddatatopkwaiting_checklist(c)
	return c.JSON(http.StatusOK, res)
}

func Update_waiting_checklist(c echo.Context) error {
	res := model.Update_waiting_checklist(c)
	return c.JSON(http.StatusOK, res)
}

func Getpkready_checklistall(c echo.Context) error {
	res := model.Getpkready_checklistall(c)
	return c.JSON(http.StatusOK, res)
}

func Getpkready_checklistbyid(c echo.Context) error {
	res := model.Getpkready_checklistbyid(c)
	return c.JSON(http.StatusOK, res)
}

func Updatepkready_checklist(c echo.Context) error {
	res := model.Updatepkready_checklist(c)
	return c.JSON(http.StatusOK, res)
}

func Adddatatosainvoice_checklist(c echo.Context) error {
	res := model.Adddatatosainvoice_checklist(c)
	return c.JSON(http.StatusOK, res)
}
